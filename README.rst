Python Adelaide website
-----------------------

This website is hosted on GitLab Pages and built using Pelican. (For now, anyway. Once we get the actual meetup off the ground, I'll probably set up something a bit nicer looking with Wagtail.)

To develop, create a Python 3.5 virtualenv, run ``pip install -r requirements.txt``, then run ``make devserver``.

Announcing a new meetup
.......................

Python Adelaide meetups need to be announced in the following places:

- This website (including the vCalendar file—see below)
- Meetup.com (including a mailout if any changes)
- The mailing list
- Slack
- Twitter
- The `City of Adelaide What's On page <http://www.cityofadelaide.com.au/whats-on>`_


Updating the vCalendar file
...........................

To add a **new event** to the vCalendar, file, copy the previous one and:

- Change the ``UID`` to a new UUID generated with ``uuidgen``
- Change the ``DTSTAMP`` and ``LAST-MODIFIED`` to the current UTC datetime (run ``date -u "+%Y%m%dT%H%M%SZ"``)
- Set the ``SEQUENCE`` to ``0``
- Update the date in ``DTSTART``, ``DTEND`` and ``SUMMARY`` (the dates are in UTC, but the day is always the same in UTC and Australia/Adelaide past 10:30am)

To update an **existing event**, do the following:

- Change the ``LAST-MODIFIED`` (but **not** ``DTSTAMP``)
- Increment ``SEQUENCE``
- Make your changes

Note that the file uses CRLF line endings; make sure your editor doesn't change this.
