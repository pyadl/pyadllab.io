:Title: Home
:URL:
:save_as: index.html
:sort: 00

Python Adelaide is a group for anybody interested in the Python Programming Language, who happen to be geographically located in close proximity to Adelaide. We'll be featuring talks from local members of the Python community and it's a great way to network, learn and make some new friends. We aim to be an inclusive group that is comfortable for people of all skill levels, age and gender.

Our next meeting will be **Wednesday, October 25** (doors open at 6:30pm, talks start at 7pm), at the **Minor Works Building**, 22 Stamford Ct (off Wright St), Adelaide. Romana Challans will discuss **Teaching Social Network Analysis with Python**, and Adam Brenecki will be presenting **One Encoding To Rule Them All: A Brief Introduction To Unicode**. You can find more details and RSVP on `Facebook <https://www.facebook.com/events/346585322473030/>`_ or `Meetup <https://www.meetup.com/en-AU/pythonadelaide/events/244109185>`_. We're looking for speakers for November, so if you've got something you'd like to share with us `please let us know! </pages/speaking.html>`_

**Getting to the venue**: There is pedestrian access to the venue from Sturt St, where you'll find ample street parking and the City South tram stop.

.. raw:: html

    <div style="font-size: 1.5em;">
        Subscribe to future meetings
        <a href="webcal://pythonadelai.de/static/calendar.ics">in your calendar</a>,
        <a href="https://www.meetup.com/pythonadelaide/">on Meetup.com</a>
        or <a href="https://www.facebook.com/pythonadelaide/">on Facebook</a>
        &bull;
        <a href="https://pythonadelaide-slack.herokuapp.com/">Chat with us on Slack</a>
        &bull;
        <a href="https://mail.python.org/mm3/mailman3/lists/python-adelaide.python.org/">Subscribe to the mailing list</a>
        &bull;
        <a href="https://goo.gl/forms/QTAVRlHz02URMYg92">Send the organisers feedback</a>
