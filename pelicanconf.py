#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Python Adelaide organisers'
SITENAME = 'Python Adelaide'
SITEURL = ''

PATH = 'content'
STATIC_PATHS = ('static',)

TIMEZONE = 'Australia/Adelaide'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ('Meetup', 'https://www.meetup.com/Python-Adelaide/'),
    ('Slack', 'https://pythonadelaide-slack.herokuapp.com/'),
    ('Mailing List', 'https://mail.python.org/mm3/mailman3/lists/python-adelaide.python.org/'),
)

MENUITEMS = LINKS

# Social widget
# SOCIAL = (('You can add links in your config file', '#'),
#           ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

PAGE_ORDER_BY = 'sort'
